Name:           cds-workstation
Version:        1.0
Release:        13%{?dist}
Summary:        Metapackage for CDS workstation packages

Group:          Applications/System
License:        Public Domain

Source0:	ligotools.sh
Requires:       ligo-ca-certs socat scipy xterm gstreamer-ffmpeg
Requires:       xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi xorg-x11-fonts-misc
Requires:       python-matplotlib gstreamer-python
Requires:       epics-extension-probe epics-extension-striptool epics-extension-burt epics-extension-medm
Requires:       pyepics grace nds2-client-python lalapps guardian mDV dataviewer
Requires:       ligo-gnome-menus ligo-proxy-utils gsi-openssh-clients
Requires:       cdsutils nds2-client-mex ligotools-tconvert
# We require a custom built gds package that includes the epics
# pieces and dmtviewer.  The epics in the version ensures that the
# CDS build is used.
#Requires:       gds-crtools = 2.17.10-2.1.el7, gds-monitors = 2.17.10-2.1.el7
Requires:       gds-crtools gds-epics

%description
%{summary}.

%prep
# Nothing to prep


%build
# Nothing to build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -m 0644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/ligotools.sh

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{_sysconfdir}/profile.d/ligotools.sh

%changelog
* Mon May 22 2017 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-13
- Add dependency on gds-epics

* Fri Feb 3 2017 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-12
- Remove gnuradio dependency (dropped from epel)

* Thu Jan 19 2017 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-11
- Remove symlink to lalapps_tconvert in favor of ligotools-tconvert

* Tue Dec 6 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-10
- Remove versioned gds dependency

* Fri Oct 28 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-9
- Add nds2-client-mex
- Update gds version dependency

* Wed Oct 19 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-8
- Update gds version dependency

* Tue Sep 27 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-7
- Update gds version dependency

* Thu Sep 8 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-6
- Update gds version dependency

* Fri May 27 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-5
- Add dataviewer, guardian, mDV dependencies

* Wed May 25 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-4
- Add symlink to tconvert binary

* Wed May 25 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-3
- Update gds version.  Add dependency on gsissh client

* Wed May 11 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-2
- Use versioned dependency for gds-crtools

* Wed May 11 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 1.0-1
- Initial package

